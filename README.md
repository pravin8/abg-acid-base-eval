#**ABG ACid-Base Eval**#

ABG Acid-Base eval walks a clinician through a stepwise approach to analyse ABG's and electrolytes in order to arrive at acid-base disorders and diagnostic ideas about ill patients.  Based mainly upon a stepwise approach from Dr. Erik Rupard, this tool not only yields answers, but just as importantly teaches the stepwise thinking process and the necessary concepts along the way.  A clinician or learner will wind up with good information that assists patient care and should wind up a little smarter and more able to do it with less assistance the next time.  It is a great support for those of us clinicians who use these tests only occasionally and don't have it memorized.   The differential diagnoses include the classic mnemonics like MUDPILERS, CHAMPS, CLEVER PD, and HARDUPS, with explanations and examples.
The app addresses such common questions as: 
•	What are common causes of metabolic acidosis? respiratory alkalosis?
•	How do I calculate predicted pCO2? corrected bicarbonate? 
•	What is the primary acid-base disorder? 
•	Are additional acid-base disorders present, and if so, what are they? 
This app is written and intended for practicing clinicians like internists, family physicians, ER docs, cardiologists, pulmonologists, intensivists, and hospitalists; for resident physician trainees; for medical students; and of course for other clinician colleagues like NP's and PA's.  As an educator and clinician, I am interested in feedback and I would be grateful for guidance on improving the tool.

Keywords: acidosis, anion gap, alkalosis, metabolic, pH, bicarbonate

Copyright: 2017 Joshua Steinberg MD and Pravin Biradar

* Version - 1.5

### Who do I talk to? ###

* Pravin Biradar
* Dr. Joshua Steinberg
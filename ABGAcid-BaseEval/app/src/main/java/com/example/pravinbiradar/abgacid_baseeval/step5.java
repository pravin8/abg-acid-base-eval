package com.example.pravinbiradar.abgacid_baseeval;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.print.PrintHelper;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class step5 extends AppCompatActivity {

    private Button calculate;
    private Button clear;
    private Button rationale;
    private Button step5_anion_gap_btn;
    private Button step5_proper_btn;

    private TextView step5_ag_val;
    private TextView step5_cl_val;
    private TextView step5_hco3_val;
    private TextView stpe5_na_val;

    String ph;
    String pco2;
    String na;
    String hco3;
    String cl;

    String user_step2;
    String user_step3;
    String user_step4;

    String ans;
    String s5;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent gt_MA = new Intent(step5.this, MainActivity.class);
                    startActivity(gt_MA);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);
                    return true;
                case R.id.navigation_read_this:

                    Intent gt_read = new Intent(step5.this, read_this.class);
                    startActivity(gt_read);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
                case R.id.navigation_about:
                    Intent gt_about = new Intent(step5.this, about.class);
                    startActivity(gt_about);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step5);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        step5_cl_val = (TextView)findViewById(R.id.step5_cl_val);
        step5_hco3_val = (TextView)findViewById(R.id.step5_hco3_val);
        stpe5_na_val = (TextView)findViewById(R.id.stpe5_na_val);




        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ph = extras.getString("ph_val");
            pco2 = extras.getString("pco2_val");
            na = extras.getString("na_val");
            hco3 = extras.getString("hco3_val");
            cl = extras.getString("cl_val");

            user_step2 = extras.getString("user_step2");
            user_step3 = extras.getString("user_step3");
            user_step4 = extras.getString("user_step4");

            step5_cl_val.setText(cl);
            step5_hco3_val.setText(hco3);
            stpe5_na_val.setText(na);

        }
        stpe5_na_val = (TextView)findViewById(R.id.stpe5_na_val);
        step5_cl_val = (TextView)findViewById(R.id.step5_cl_val);
        step5_hco3_val = (TextView)findViewById(R.id.step5_hco3_val);
        step5_ag_val = (TextView)findViewById(R.id.step5_ag_val);

        calculate = (Button)findViewById(R.id.step5_calculate);
        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Float v4 = Float.parseFloat(na);
                Float v5 = Float.parseFloat(cl);
                Float v6 = Float.parseFloat(hco3);

                Float x = (v4-v5-v6) ;
                ans = x.toString();
                step5_ag_val.setText(ans);

            }
        });


        clear = (Button)findViewById(R.id.step5_clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                step5_ag_val.setText(" ");
            }
        });

        rationale = (Button)findViewById(R.id.step5_rationale);
        rationale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gt_rationale = new Intent(step5.this, step5_rationale.class);
                startActivity(gt_rationale);
            }
        });

        step5_anion_gap_btn = (Button)findViewById(R.id.step5_anion_gap_btn);
        step5_anion_gap_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent transition = new Intent(step5.this, step6.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);

                transition.putExtra("ag_val",ans);

                transition.putExtra("user_step2", user_step2);
                transition.putExtra("user_step3", user_step3);
                transition.putExtra("user_step4", user_step4);
                s5 = "Anion Gap Metabolic Acidosis";
                transition.putExtra("user_step5", s5);

                startActivity(transition);
            }
        });

        step5_proper_btn = (Button)findViewById(R.id.step5_proper_btn);
        step5_proper_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step5.this, step6.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);
                transition.putExtra("ag_val",ans);

                transition.putExtra("user_step2", user_step2);
                transition.putExtra("user_step3", user_step3);
                transition.putExtra("user_step4", user_step4);

                s5 = "No Anion Gap";
                transition.putExtra("user_step5", s5);

                startActivity(transition);
            }
        });
    }

}

package com.example.pravinbiradar.abgacid_baseeval;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class step3_alk extends AppCompatActivity {

    private Button rationale;
    private Button metabolic_alkalosis;
    private Button respiratory_alkalosis;

    private TextView step3_alk_pco2_value;

    String ph;
    String pco2;
    String na;
    String hco3;
    String cl;
    String user_step2;

    String s3;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent gt_MA = new Intent(step3_alk.this, MainActivity.class);
                    startActivity(gt_MA);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);
                    return true;
                case R.id.navigation_read_this:

                    Intent gt_read = new Intent(step3_alk.this, read_this.class);
                    startActivity(gt_read);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
                case R.id.navigation_about:
                    Intent gt_about = new Intent(step3_alk.this, about.class);
                    startActivity(gt_about);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step3_alk);



        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ph = extras.getString("ph_val");
            pco2 = extras.getString("pco2_val");
            na = extras.getString("na_val");
            hco3 = extras.getString("hco3_val");
            cl = extras.getString("cl_val");
            user_step2 = extras.getString("user_step2");

            step3_alk_pco2_value = (TextView)findViewById(R.id.step3_alk_pco2_value);
            step3_alk_pco2_value.setText(pco2);

        }

        rationale = (Button)findViewById(R.id.rationale_step3_alk);
        rationale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gt_rationale = new Intent(step3_alk.this, step3_alk_rationale.class);
                startActivity(gt_rationale);
            }
        });


        metabolic_alkalosis = (Button)findViewById(R.id.metabolic_alkalosis_btn);
        metabolic_alkalosis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gt_step4 = new Intent(step3_alk.this, step4_met_alk.class);
                gt_step4.putExtra("ph_val", ph);
                gt_step4.putExtra("pco2_val", pco2);
                gt_step4.putExtra("na_val", na);
                gt_step4.putExtra("hco3_val", hco3);
                gt_step4.putExtra("cl_val", cl);

                gt_step4.putExtra("user_step2", user_step2);
                s3 = "Metabolic Alkalosis";
                gt_step4.putExtra("user_step3", s3);

                startActivity(gt_step4);

            }
        });


        respiratory_alkalosis = (Button)findViewById(R.id.respiratory_alkalosis_btn);
        respiratory_alkalosis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gt_step4 = new Intent(step3_alk.this, step4_resp.class);
                gt_step4.putExtra("ph_val", ph);
                gt_step4.putExtra("pco2_val", pco2);
                gt_step4.putExtra("na_val", na);
                gt_step4.putExtra("hco3_val", hco3);
                gt_step4.putExtra("cl_val", cl);
                gt_step4.putExtra("primary_disorder",respiratory_alkalosis.getText());

                gt_step4.putExtra("user_step2", user_step2);
                s3 = "Respiratory Alkalosis";
                gt_step4.putExtra("user_step3", s3);


                startActivity(gt_step4);

            }
        });

    }


}

package com.example.pravinbiradar.abgacid_baseeval;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class step4_met_alk extends AppCompatActivity {

    private Button note;
    private Button rationale;
    private Button add_resp_acid_btn;
    private Button add_resp_alk_btn;
    private Button proper_disorder_btn;

    private TextView step4_met_alk_pco2_value;

    String ph;
    String pco2;
    String na;
    String hco3;
    String cl;
    String user_step2;
    String user_step3;

    String s4;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent gt_MA = new Intent(step4_met_alk.this, MainActivity.class);
                    startActivity(gt_MA);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);
                    return true;
                case R.id.navigation_read_this:

                    Intent gt_read = new Intent(step4_met_alk.this, read_this.class);
                    startActivity(gt_read);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
                case R.id.navigation_about:
                    Intent gt_about = new Intent(step4_met_alk.this, about.class);
                    startActivity(gt_about);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step4_met_alk);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ph = extras.getString("ph_val");
            pco2 = extras.getString("pco2_val");
            na = extras.getString("na_val");
            hco3 = extras.getString("hco3_val");
            cl = extras.getString("cl_val");

            user_step2 = extras.getString("user_step2");
            user_step3 = extras.getString("user_step3");


            step4_met_alk_pco2_value = (TextView)findViewById(R.id.step4_met_alk_pco2_value);
            step4_met_alk_pco2_value.setText(pco2);

        }

        note = (Button)findViewById(R.id.step4_note);
        note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gt_note = new Intent(step4_met_alk.this, step4_met_alk_rationale.class);
                startActivity(gt_note);
            }
        });

        rationale = (Button)findViewById(R.id.step4_rationale);
        rationale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gt_note = new Intent(step4_met_alk.this, step4_met_alk_rationale.class);
                startActivity(gt_note);
            }
        });


        add_resp_acid_btn = (Button)findViewById(R.id.add_resp_acid_btn);
        add_resp_acid_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step4_met_alk.this,step5.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);

                transition.putExtra("user_step2", user_step2);
                transition.putExtra("user_step3", user_step3);
                s4 = "Respiratory Acidosis";
                transition.putExtra("user_step4", s4);


                startActivity(transition);

            }
        });


        add_resp_alk_btn = (Button)findViewById(R.id.add_resp_alk_btn);
        add_resp_alk_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step4_met_alk.this,step5.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);

                transition.putExtra("user_step2", user_step2);
                transition.putExtra("user_step3", user_step3);
                s4 = "Respiratory Alkalosis";
                transition.putExtra("user_step4", s4);

                startActivity(transition);

            }
        });


        proper_disorder_btn = (Button)findViewById(R.id.proper_disorder_btn);
        proper_disorder_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step4_met_alk.this,step5.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);

                transition.putExtra("user_step2", user_step2);
                transition.putExtra("user_step3", user_step3);

                s4 = "No Disorder";
                transition.putExtra("user_step4", s4);

                startActivity(transition);

            }
        });



    }

}

package com.example.pravinbiradar.abgacid_baseeval;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.pravinbiradar.abgacid_baseeval.R.string.metabolic_alkalosis;
import static com.example.pravinbiradar.abgacid_baseeval.R.string.respiratory_alkalosis;

public class step3_acid extends AppCompatActivity {

    private Button rationale_step3_acid;
    private Button s3_met_acid_acidosis;
    private Button respiratory_acidosis_btn;

    private TextView step3_acid_pco2_value;

    String ph;
    String pco2;
    String na;
    String hco3;
    String cl;
    String user_step2;

    String s3;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent gt_MA = new Intent(step3_acid.this, MainActivity.class);
                    startActivity(gt_MA);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);
                    return true;
                case R.id.navigation_read_this:

                    Intent gt_read = new Intent(step3_acid.this, read_this.class);
                    startActivity(gt_read);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
                case R.id.navigation_about:
                    Intent gt_about = new Intent(step3_acid.this, about.class);
                    startActivity(gt_about);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step3_acid);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ph = extras.getString("ph_val");
            pco2 = extras.getString("pco2_val");
            na = extras.getString("na_val");
            hco3 = extras.getString("hco3_val");
            cl = extras.getString("cl_val");
            user_step2 = extras.getString("user_step2");


            step3_acid_pco2_value = (TextView)findViewById(R.id.step3_acid_pco2_value);
            step3_acid_pco2_value.setText(pco2);

        }

        rationale_step3_acid = (Button)findViewById(R.id.rationale_step3_acid);
        rationale_step3_acid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gt_rationale = new Intent(step3_acid.this, step3_acid_rationale.class);
                startActivity(gt_rationale);
            }
        });

        s3_met_acid_acidosis = (Button)findViewById(R.id.s3_met_acid_acidosis);
        s3_met_acid_acidosis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gt_step4 = new Intent(step3_acid.this,step4_met_acid.class);
                startActivity(gt_step4);
                gt_step4.putExtra("ph_val", ph);
                gt_step4.putExtra("pco2_val", pco2);
                gt_step4.putExtra("na_val", na);
                gt_step4.putExtra("hco3_val", hco3);
                gt_step4.putExtra("cl_val", cl);

                gt_step4.putExtra("user_step2", user_step2);
                s3 = "Metabolic Acidosis";
                gt_step4.putExtra("user_step3", s3);
                startActivity(gt_step4);

            }
        });




        respiratory_acidosis_btn = (Button)findViewById(R.id.respiratory_acidosis_btn);
        respiratory_acidosis_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step3_acid.this, step4_resp.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);
                transition.putExtra("primary_disorder",respiratory_acidosis_btn.getText());

                transition.putExtra("user_step2", user_step2);
                s3 = "Respiratory Acidosis";
                transition.putExtra("user_step3", s3);
                startActivity(transition);

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent go_back = new Intent(step3_acid.this, step2.class);
        go_back.putExtra("ph_val", ph);
        go_back.putExtra("pco2_val", pco2);
        go_back.putExtra("na_val", na);
        go_back.putExtra("hco3_val", hco3);
        go_back.putExtra("cl_val", cl);
        startActivity(go_back);
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);


    }
}

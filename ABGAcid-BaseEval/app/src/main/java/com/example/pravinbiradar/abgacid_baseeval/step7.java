package com.example.pravinbiradar.abgacid_baseeval;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class step7 extends AppCompatActivity {

    private Button step7_compare;
    private Button step7_Note;
    private Button step7_ag_met_acid;
    private Button step7_resp_acid;
    private Button step7_non_an_gap;
    private Button step7_met_alk;
    private Button step7_resp_alk;
    private Button go_to_step8;

    TextView step7_step23;
    TextView step7_step4;
    TextView step7_step5;
    TextView step7_step6;


    String ph;
    String pco2;
    String na;
    String hco3;
    String cl;
    String ag;

    String user_step2;
    String user_step3;
    String user_step4;
    String user_step5;
    String user_step6;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent gt_MA = new Intent(step7.this, MainActivity.class);
                    startActivity(gt_MA);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);
                    return true;
                case R.id.navigation_read_this:

                    Intent gt_read = new Intent(step7.this, read_this.class);
                    startActivity(gt_read);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
                case R.id.navigation_about:
                    Intent gt_about = new Intent(step7.this, about.class);
                    startActivity(gt_about);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step7);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        step7_step23 = (TextView)findViewById(R.id.step7_step23);
        step7_step4 = (TextView)findViewById(R.id.step7_step4);
        step7_step5 = (TextView)findViewById(R.id.step7_step5);
        step7_step6 = (TextView)findViewById(R.id.step7_step6);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ph = extras.getString("ph_val");
            pco2 = extras.getString("pco2_val");
            na = extras.getString("na_val");
            hco3 = extras.getString("hco3_val");
            cl = extras.getString("cl_val");
            ag = extras.getString("ag_val");

            user_step2 = extras.getString("user_step2");
            user_step3 = extras.getString("user_step3");
            user_step4 = extras.getString("user_step4");
            user_step5 = extras.getString("user_step5");
            user_step6 = extras.getString("user_step6");

            step7_step23.setText(user_step3);
            step7_step4.setText(user_step4);
            step7_step5.setText(user_step5);
            step7_step6.setText(user_step6);


        }

        step7_compare = (Button)findViewById(R.id.step7_compare);
        step7_compare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step7.this,step7_compare.class);

                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);

                transition.putExtra("user_step2", user_step2);
                transition.putExtra("user_step3", user_step3);
                transition.putExtra("user_step4", user_step4);
                transition.putExtra("user_step5", user_step5);
                transition.putExtra("user_step6", user_step6);

                transition.putExtra("ag_val",ag);
                startActivity(transition);
            }
        });

        step7_Note = (Button)findViewById(R.id.step7_Note);
        step7_Note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step7.this,step7_note.class);
                startActivity(transition);
            }
        });

        step7_ag_met_acid = (Button)findViewById(R.id.step7_ag_met_acid);
        step7_ag_met_acid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step7.this,ag_met_acid_ddx.class);
                startActivity(transition);
            }
        });

        step7_resp_acid = (Button)findViewById(R.id.step7_resp_acid);
        step7_resp_acid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step7.this,respiratory_acid_ddx.class);
                startActivity(transition);
            }
        });

        step7_non_an_gap = (Button)findViewById(R.id.step7_non_an_gap);
        step7_non_an_gap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step7.this,non_gap_met_acid_ddx.class);
                startActivity(transition);
            }
        });

        step7_met_alk = (Button)findViewById(R.id.step7_met_alk);
        step7_met_alk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step7.this,metabolic_alkalosis.class);
                startActivity(transition);
            }
        });

        step7_resp_alk = (Button)findViewById(R.id.step7_resp_alk);
        step7_resp_alk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step7.this,resp_alk_ddx.class);
                startActivity(transition);
            }
        });

        go_to_step8 = (Button)findViewById(R.id.go_to_step8);
        go_to_step8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step7.this,step8.class);
                startActivity(transition);
            }
        });

    }

}

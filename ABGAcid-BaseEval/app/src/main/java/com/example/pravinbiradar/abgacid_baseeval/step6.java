package com.example.pravinbiradar.abgacid_baseeval;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class step6 extends AppCompatActivity {

    private Button calculate;
    private Button clear;
    private Button rationale;
    private Button step6_non_anion_gap_btn;
    private Button step6_no_abn_btn;
    private Button step6_met_alk_btn;

    private TextView step6_cb_val;
    private TextView step6_ag_val;
    private TextView step6_hco3_val;

    String ph;
    String pco2;
    String na;
    String hco3;
    String cl;

    String user_step2;
    String user_step3;
    String user_step4;
    String user_step5;

    String final_ag_val;

    String s6;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                Intent gt_MA = new Intent(step6.this, MainActivity.class);
                startActivity(gt_MA);
                overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);
                return true;
                case R.id.navigation_read_this:

                    Intent gt_read = new Intent(step6.this, read_this.class);
                    startActivity(gt_read);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
                case R.id.navigation_about:
                    Intent gt_about = new Intent(step6.this, about.class);
                    startActivity(gt_about);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step6);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        step6_ag_val = (TextView)findViewById(R.id.step6_ag_val);
        step6_hco3_val = (TextView)findViewById(R.id.step6_hco3_val);
        step6_cb_val = (TextView)findViewById(R.id.step6_cb_val);

        calculate = (Button)findViewById(R.id.step6_calculate);
        clear = (Button)findViewById(R.id.step6_clear);




        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ph = extras.getString("ph_val");
            pco2 = extras.getString("pco2_val");
            na = extras.getString("na_val");
            hco3 = extras.getString("hco3_val");
            cl = extras.getString("cl_val");

            user_step2 = extras.getString("user_step2");
            user_step3 = extras.getString("user_step3");
            user_step4 = extras.getString("user_step4");
            user_step5 = extras.getString("user_step5");


            Float x1 = Float.parseFloat(na);
            Float x2 = Float.parseFloat(cl);
            Float x3 = Float.parseFloat(hco3);

            Float ag = (x1-x2-x3) ;

            final_ag_val = ag.toString();
            step6_ag_val.setText(final_ag_val);
            step6_hco3_val.setText(hco3);

        }

        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Float x1 = Float.parseFloat(na);
                Float x2 = Float.parseFloat(cl);
                Float x3 = Float.parseFloat(hco3);

                Float ag = (x1-x2-x3) ;

                Float v5 = ag - 12 + x3;
                String ans = v5.toString();

                step6_cb_val.setText(ans);
            }
        });


        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                step6_cb_val.setText(" ");
            }
        });

        rationale = (Button)findViewById(R.id.step6_rationale);
        rationale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gt_rationale = new Intent(step6.this, step6_rationale.class);
                startActivity(gt_rationale);
            }
        });

        step6_non_anion_gap_btn = (Button)findViewById(R.id.step6_non_anion_gap_btn);
        step6_non_anion_gap_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step6.this, step7.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);

                transition.putExtra("user_step2", user_step2);
                transition.putExtra("user_step3", user_step3);
                transition.putExtra("user_step4", user_step4);
                transition.putExtra("user_step5", user_step5);
                s6 = "Non Anion Gap Met Acidosis";
                transition.putExtra("user_step6", s6);
                transition.putExtra("ag_val",final_ag_val);

                startActivity(transition);
            }
        });

        step6_no_abn_btn = (Button)findViewById(R.id.step6_no_abn_btn);
        step6_no_abn_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step6.this, step7.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);

                transition.putExtra("user_step2", user_step2);
                transition.putExtra("user_step3", user_step3);
                transition.putExtra("user_step4", user_step4);
                transition.putExtra("user_step5", user_step5);
                s6 = "No Disorder";
                transition.putExtra("user_step6", s6);
                transition.putExtra("ag_val",final_ag_val);

                startActivity(transition);
            }
        });

        step6_met_alk_btn = (Button)findViewById(R.id.step6_met_alk_btn);
        step6_met_alk_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step6.this, step7.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);

                transition.putExtra("user_step2", user_step2);
                transition.putExtra("user_step3", user_step3);
                transition.putExtra("user_step4", user_step4);
                transition.putExtra("user_step5", user_step5);
                s6 = "Metabolic Alkalosis";
                transition.putExtra("user_step6", s6);
                transition.putExtra("ag_val",final_ag_val);

                startActivity(transition);
            }
        });
    }

}

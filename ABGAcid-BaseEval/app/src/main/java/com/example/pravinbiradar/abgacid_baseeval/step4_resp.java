package com.example.pravinbiradar.abgacid_baseeval;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class step4_resp extends AppCompatActivity {

    private TextView primary_disorder;
    private Button gt_s5;

    String ph;
    String pco2;
    String na;
    String hco3;
    String cl;

    String user_step2;
    String user_step3;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent gt_MA = new Intent(step4_resp.this, MainActivity.class);
                    startActivity(gt_MA);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);
                    return true;
                case R.id.navigation_read_this:

                    Intent gt_read = new Intent(step4_resp.this, read_this.class);
                    startActivity(gt_read);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
                case R.id.navigation_about:
                    Intent gt_about = new Intent(step4_resp.this, about.class);
                    startActivity(gt_about);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step4_resp);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        primary_disorder = (TextView)findViewById(R.id.primary_disorder);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            String pd = extras.getString("primary_disorder");
            primary_disorder.setText(pd);

            ph = extras.getString("ph_val");
            pco2 = extras.getString("pco2_val");
            na = extras.getString("na_val");
            hco3 = extras.getString("hco3_val");
            cl = extras.getString("cl_val");

            user_step2 = extras.getString("user_step2");
            user_step3 = extras.getString("user_step3");

        }

        gt_s5 = (Button)findViewById(R.id.gt_s5);
        gt_s5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step4_resp.this, step5.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);

                transition.putExtra("user_step2", user_step2);
                transition.putExtra("user_step3", user_step3);
                transition.putExtra("user_step4", "No Disorder");

                startActivity(transition);
            }
        });
    }

}

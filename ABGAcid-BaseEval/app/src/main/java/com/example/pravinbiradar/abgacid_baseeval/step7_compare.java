package com.example.pravinbiradar.abgacid_baseeval;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

public class step7_compare extends AppCompatActivity {

    TextView s7c_user3;
    TextView s7c_user4;
    TextView s7c_user5;
    TextView s7c_user6;

    TextView app_step3_val;
    TextView app_step4_val;
    TextView app_step5_val;
    TextView app_step6_val;



    String ph;
    String pco2;
    String na;
    String hco3;
    String cl;
    String ag;

    String user_step2;
    String user_step3;
    String user_step4;
    String user_step5;
    String user_step6;

    String app_step2;
    String app_step3;
    String app_step4;
    String app_step5;
    String app_step6;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent gt_MA = new Intent(step7_compare.this, MainActivity.class);
                    startActivity(gt_MA);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);
                    return true;
                case R.id.navigation_read_this:

                    Intent gt_read = new Intent(step7_compare.this, read_this.class);
                    startActivity(gt_read);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);
                    return true;
                case R.id.navigation_about:
                    Intent gt_about = new Intent(step7_compare.this, about.class);
                    startActivity(gt_about);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step7_compare);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        s7c_user3  = (TextView)findViewById(R.id.s7c_user3);
        s7c_user4  = (TextView)findViewById(R.id.s7c_user4);
        s7c_user5  = (TextView)findViewById(R.id.s7c_user5);
        s7c_user6  = (TextView)findViewById(R.id.s7c_user6);

        app_step3_val = (TextView)findViewById(R.id.app_step3);
        app_step4_val = (TextView)findViewById(R.id.app_step4);
        app_step5_val = (TextView)findViewById(R.id.app_step5);
        app_step6_val = (TextView)findViewById(R.id.app_step6);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ph = extras.getString("ph_val");
            pco2 = extras.getString("pco2_val");
            na = extras.getString("na_val");
            hco3 = extras.getString("hco3_val");
            cl = extras.getString("cl_val");
            ag = extras.getString("ag_val");

            user_step2 = extras.getString("user_step2");
            user_step3 = extras.getString("user_step3");
            user_step4 = extras.getString("user_step4");
            user_step5 = extras.getString("user_step5");
            user_step6 = extras.getString("user_step6");

            s7c_user3.setText(user_step3);
            s7c_user4.setText(user_step4);
            s7c_user5.setText(user_step5);
            s7c_user6.setText(user_step6);

            Double x1 = Double.parseDouble(ph);
            Double x2 = Double.parseDouble(pco2);
            Double x3 = Double.parseDouble(na);
            Double x4 = Double.parseDouble(hco3);
            Double x5 = Double.parseDouble(cl);
            Double x6 = Double.parseDouble(ag);

            Double pred_pco2 = (1.5 * x4) + 8 ;

            Double cb = x6 - 12 + x4;


            //if pH > 7.40
            if (x1 > 7.40){
                String app_step2 = "Alkalosis";
                // pCO2 < 40
                if(x2 < 40){
                    app_step3 = "Respiratory Alkalosis";
                    app_step4 = "No Disorder";
                }
                //pCO2 >= 40
                else{
                    app_step3 = "Metabolic Alkalosis";
                    //pCO2 > 55
                    if(x2>55) {
                        app_step4 = "Respiratory Acidosis";
                    }
                    // pCO2 < 41
                    else if(x2<41){
                        app_step4 = "Respiratory Alkalosis";
                    }
                    // pCO2 is between 41 and 55
                    else{
                        app_step4 = "No Disorder";
                    }
                }
            }
            // if pH = 7.40
            else if (x1 == 7.40){
                app_step2 = "No Disorder";
                app_step3 = "No Disorder";
                app_step4 = "No Disorder";

            }
            //ph < 7.40
            else{
                app_step2 = "Acidosis";
                //pCO2 > 40
                if (x2>40){app_step3 = "Respiratory Acidosis";
                    app_step4 = "No Disorder";}
                //pCO2 < 40
                else{
                    app_step3 = "Metabolic Acidosis";
                    //if pCO2 > (predicted pCO2 + 2), return too high
                    if(x2 > (pred_pco2+2)){
                        app_step4 = "Respiratory Acidoosis";
                    }
                    //if pCO2 < (predicted pCO2 - 2), return too low
                    else if(x2 < (pred_pco2 - 2)){
                        app_step4 = "Respiratory Alkalosis";
                    }
                    //else return normal
                    else{app_step4= "No disorder";}

                }

            }

            //anion gap greater than 14
            if(x6 >14){

                app_step5 = "Anion Gap Metabolic Acidosis";
            }
            else{

                app_step5 = "No Anion Gap Disorder";
            }

            //if cb>30
            if(cb > 30){
                app_step6 = "Metabolic Alkalosis";

            }
            //cb>23
            else if(cb<23) {
                app_step6 = "Non Anion Gap Met Acidosis";
            }
            //cb is between 23 and 30
            else{
                app_step6 = "No Disorder";
            }

            app_step3_val.setText(app_step3);
            app_step4_val.setText(app_step4);
            app_step5_val.setText(app_step5);
            app_step6_val.setText(app_step6);


        }
    }

}

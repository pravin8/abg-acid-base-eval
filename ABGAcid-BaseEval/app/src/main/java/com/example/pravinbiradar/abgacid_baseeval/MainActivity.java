package com.example.pravinbiradar.abgacid_baseeval;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView mScreenTitle;
    private TextView mMainText;
    public Button step2Btn;
    public Button reset;

    public NumberPicker ph_picker;
    public NumberPicker pco2_picker;
    public NumberPicker na_picker ;
    public NumberPicker hco3_picker;
    public NumberPicker cl_picker ;

    String[] phValues = {"6.80","6.81","6.82","6.83","6.84","6.85","6.86","6.87","6.88","6.89","6.90","6.91","6.92","6.93","6.94","6.95","6.96","6.97","6.98","6.99","7.00","7.01","7.02","7.03","7.04","7.05","7.06","7.07","7.08","7.09","7.10","7.11","7.12","7.13","7.14","7.15","7.16","7.17","7.18","7.19","7.20","7.21","7.22","7.23","7.24","7.25","7.26","7.27","7.28","7.29","7.30","7.31","7.32","7.33","7.34","7.35","7.36","7.37","7.38","7.39","7.40","7.41","7.42","7.43","7.44","7.45","7.46","7.47","7.48","7.49","7.50","7.51","7.52","7.53","7.54","7.55","7.56","7.57","7.58","7.59","7.60","7.61","7.62","7.63","7.64","7.65","7.66","7.67","7.68","7.69","7.70"};
    String[] pco2Values = {"10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108","109","110"};
    String[] naValues = {"110","111","112","113","114","115","116","117","118","119","120","121","122","123","124","125","126","127","128","129","130","131","132","133","134","135","136","137","138","139","140","141","142","143","144","145","146","147","148","149","150","151","152","153","154","155","156","157","158","159","160","161","162","163","164","165","166","167","168","169","170"};
    String[] hco3Values = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50"};
    String[] clValues = {"70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108","109","110","111","112","113","114","115","116","117","118","119","120","121","122","123","124","125","126","127","128","129","130"};

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    return true;
                case R.id.navigation_read_this:

                    Intent gt_read = new Intent(MainActivity.this, read_this.class);
                    startActivity(gt_read);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
                case R.id.navigation_about:

                    Intent gt_about = new Intent(MainActivity.this, about.class);
                    startActivity(gt_about);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mScreenTitle = (TextView) findViewById(R.id.screen_title);
        mMainText = (TextView) findViewById(R.id.main_text);
        step2Btn = (Button) findViewById(R.id.gt_s2_btn);
        reset = (Button)findViewById(R.id.reset) ;
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        step2Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ph =  phValues[ph_picker.getValue()];
                String pco2 = pco2Values[pco2_picker.getValue()];
                String na = naValues[na_picker.getValue()];
                String hco3 = hco3Values[hco3_picker.getValue()];
                String cl = clValues[cl_picker.getValue()];

                Intent transition = new Intent(MainActivity.this,step2.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);

                startActivity(transition);


            }
        });



        ph_picker =(NumberPicker) findViewById(R.id.ph_Picker);
        pco2_picker = (NumberPicker) findViewById(R.id.pco2_Picker);
        na_picker = (NumberPicker) findViewById(R.id.na_Picker);
        hco3_picker = (NumberPicker) findViewById(R.id.hco3_Picker);
        cl_picker = (NumberPicker) findViewById(R.id.cl_Picker);


        ph_picker.setMinValue(0);
        ph_picker.setMaxValue(phValues.length -1);
        ph_picker.setWrapSelectorWheel(false);
        ph_picker.setDisplayedValues(phValues);
        ph_picker.setDescendantFocusability(ph_picker.FOCUS_BLOCK_DESCENDANTS);


        pco2_picker.setDisplayedValues(pco2Values);
        pco2_picker.setMinValue(0);
        pco2_picker.setMaxValue(pco2Values.length -1);
        pco2_picker.setWrapSelectorWheel(false);
        pco2_picker.setDescendantFocusability(pco2_picker.FOCUS_BLOCK_DESCENDANTS);


        na_picker.setDisplayedValues(naValues);
        na_picker.setMinValue(0);
        na_picker.setMaxValue(naValues.length-1);
        na_picker.setWrapSelectorWheel(false);
        na_picker.setDescendantFocusability(na_picker.FOCUS_BLOCK_DESCENDANTS);


        hco3_picker.setDisplayedValues(hco3Values);
        hco3_picker.setMinValue(0);
        hco3_picker.setMaxValue(hco3Values.length-1);
        hco3_picker.setWrapSelectorWheel(false);
        hco3_picker.setDescendantFocusability(hco3_picker.FOCUS_BLOCK_DESCENDANTS);

        cl_picker.setDisplayedValues(clValues);
        cl_picker.setMinValue(0);
        cl_picker.setMaxValue(clValues.length-1);
        cl_picker.setWrapSelectorWheel(false);
        cl_picker.setDescendantFocusability(cl_picker.FOCUS_BLOCK_DESCENDANTS);


        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ph_picker.setValue(60);
                pco2_picker.setValue(30);
                na_picker.setValue(30);
                hco3_picker.setValue(23);
                cl_picker.setValue(34);
            }
        });

    }


    private Boolean exit = false;
    @Override
    public void onBackPressed() {
        if (exit) {
            moveTaskToBack(true);
            finish(); // finish activity
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }


    }

    public static boolean isInFront;

    @Override
    protected void onResume() {
        super.onResume();

        isInFront = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isInFront = false;
    }
}

package com.example.pravinbiradar.abgacid_baseeval;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class step4_met_acid extends AppCompatActivity {

    private Button s4macid_calc;
    private Button s4macid_clear;
    private Button s4macid_rationale;
    private Button s4macid_add_resp_acid_btn;
    private Button s4macid_add_resp_alk_btn;
    private Button s4macid_proper_disorder_btn;

    private TextView s4macid_calc_view;
    private TextView s4macid_hco3;
    private TextView s4macid_act_pco2;

    String ph;
    String pco2;
    String na;
    String hco3;
    String cl;
    String user_step2;
    String user_step3;
    String s4;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent gt_MA = new Intent(step4_met_acid.this, MainActivity.class);
                    startActivity(gt_MA);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);
                    return true;
                case R.id.navigation_read_this:

                    Intent gt_read = new Intent(step4_met_acid.this, read_this.class);
                    startActivity(gt_read);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
                case R.id.navigation_about:
                    Intent gt_about = new Intent(step4_met_acid.this, about.class);
                    startActivity(gt_about);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);

                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step4_met_acid);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        s4macid_calc_view = (TextView)findViewById(R.id.s4macid_calc_view);
        s4macid_hco3 = (TextView)findViewById(R.id.s4macid_hco3);
        s4macid_act_pco2 = (TextView)findViewById(R.id.s4macid_act_pco2);

        s4macid_calc = (Button)findViewById(R.id.s4macid_calculate);
        s4macid_clear =(Button)findViewById(R.id.s4macid_clear);
        s4macid_rationale =(Button)findViewById(R.id.s4macid_rationale);
        s4macid_add_resp_acid_btn =(Button)findViewById(R.id.s4macid_add_resp_acid_btn);
        s4macid_add_resp_alk_btn =(Button)findViewById(R.id.s4macid_add_resp_alk_btn);
        s4macid_proper_disorder_btn =(Button)findViewById(R.id.s4macid_proper_disorder_btn);

        s4macid_hco3.setText(hco3);
        s4macid_act_pco2.setText(pco2);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ph = extras.getString("ph_val");
            pco2 = extras.getString("pco2_val");
            na = extras.getString("na_val");
            hco3 = extras.getString("hco3_val");
            cl = extras.getString("cl_val");

            user_step2 = extras.getString("user_step2");
            user_step3 = extras.getString("user_step3");

            s4macid_hco3.setText(hco3);
            s4macid_act_pco2.setText(pco2);


        }


        s4macid_calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String v1 = s4macid_hco3.getText().toString();
                Float v2 = Float.parseFloat(v1);
                Double v3 = (1.5 * v2) + 8;
                String ans = v3.toString();

                s4macid_calc_view.setText(ans);


            }
        });



        s4macid_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s4macid_calc_view.setText(" ");
            }
        });


        s4macid_rationale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gt_rationale = new Intent(step4_met_acid.this, step4_met_acid_rationale.class);
                startActivity(gt_rationale);
            }
        });

        s4macid_add_resp_acid_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step4_met_acid.this, step5.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);

                transition.putExtra("user_step2", user_step2);
                transition.putExtra("user_step3", user_step3);
                s4 = "Respiratory Acidosis";
                transition.putExtra("user_step4", s4);

                startActivity(transition);
            }
        });

        s4macid_add_resp_alk_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step4_met_acid.this, step5.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);

                transition.putExtra("user_step2", user_step2);
                transition.putExtra("user_step3", user_step3);
                s4 = "Respiratory Alkalosis";
                transition.putExtra("user_step4", s4);

                startActivity(transition);
            }
        });

        s4macid_proper_disorder_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step4_met_acid.this, step5.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);

                transition.putExtra("user_step2", user_step2);
                transition.putExtra("user_step3", user_step3);
                s4 = "No Disorder";
                transition.putExtra("user_step4", s4);

                startActivity(transition);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent transition = new Intent(step4_met_acid.this, step3_acid.class);
        transition.putExtra("ph_val", ph);
        transition.putExtra("pco2_val", pco2);
        transition.putExtra("na_val", na);
        transition.putExtra("hco3_val", hco3);
        transition.putExtra("cl_val", cl);

        transition.putExtra("user_step2", user_step2);
        transition.putExtra("user_step3", user_step3);


        startActivity(transition);
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);



    }
}

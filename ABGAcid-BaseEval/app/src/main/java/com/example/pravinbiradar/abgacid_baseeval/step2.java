package com.example.pravinbiradar.abgacid_baseeval;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class step2 extends AppCompatActivity {

    private Button alkalosis_btn;
    private Button acidosis_btn;
    private Button neutral_btn;
    private TextView phvalue;

    String ph;
    String pco2;
    String na;
    String hco3;
    String cl;

    String s2;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent gt_MA = new Intent(step2.this, MainActivity.class);
                    startActivity(gt_MA);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);
                    return true;
                case R.id.navigation_read_this:
                    Intent gt_read = new Intent(step2.this, read_this.class);
                    startActivity(gt_read);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);
                    return true;
                case R.id.navigation_about:
                    Intent gt_about = new Intent(step2.this, about.class);
                    startActivity(gt_about);
                    overridePendingTransition(R.anim.as_it_is,R.anim.slide_in_up);
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step2);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ph = extras.getString("ph_val");
            pco2 = extras.getString("pco2_val");
            na = extras.getString("na_val");
            hco3 = extras.getString("hco3_val");
            cl = extras.getString("cl_val");
            phvalue = (TextView)findViewById(R.id.step2_ph_value);
            phvalue.setText(ph);
        }


        alkalosis_btn = (Button)findViewById(R.id.alkalosis_btn);
        alkalosis_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step2.this,step3_alk.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);
                s2 = "Alkalosis";
                transition.putExtra("user_step2", s2);

                startActivity(transition);

            }
        });


        acidosis_btn = (Button)findViewById(R.id.acidosis_btn);
        acidosis_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step2.this,step3_acid.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);
                s2 = "Acidosis";
                transition.putExtra("user_step2", s2);


                startActivity(transition);

            }
        });


        neutral_btn = (Button)findViewById(R.id.neutral_btn);
        neutral_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(step2.this,ph74.class);
                transition.putExtra("ph_val", ph);
                transition.putExtra("pco2_val", pco2);
                transition.putExtra("na_val", na);
                transition.putExtra("hco3_val", hco3);
                transition.putExtra("cl_val", cl);
                s2 = "Neutral";
                transition.putExtra("user_step2", s2);
                startActivity(transition);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent go_back = new Intent(step2.this, MainActivity.class);

        startActivity(go_back);
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }
}
